# Pràctica Proxy Squid 1:

Ara que ja saps instal·lar i configurar el proxy Squid [recorda el tutorial que tens aquí](https://dungeonofbits.com/instalacion-de-squid-en-linux.html) podem començar a treballar en el nostre propi proxy.

Per orientar-te tens la documentació de Squid [aquí](https://wiki.squid-cache.org/SquidFaq/SquidAcl#Access_Controls_in_Squid)

Potser hauràs de canviar la configuració de squid.conf per a cada punt, així que serà millor que guardis una copia de seguretat cada vegada amb la configuració funcionant i facis les captures abans de canviar-la.

## 1.- Instal·lació:

	Instal·la Squid (mostra una captura del proxy funcionant i que al terminal es vegi el teu usuari):
	
	

![](Imagenes/Squid/1-Prompt.png)
	

## 2.- Configurar accés per IP:

Mostra captures que demostrin els següents punts:

* Crea un fitxer anomenat permesos.txt amb les IPs dels equips de dos companys de classe.
* Crea un fitxer anomenat prohibits.txt amb les IPs dels equips de dos companys de classe.
* Crea una ACL anomenada X_seal_of_aprovement (on X és el teu cognom) amb les IPs del fitxer permesos.txt

* Crea una ACL anomenada X_death_note (on X és el teu cognom) amb les IPs del fitxer prohibits.txt

* Permet les connexions dels equips de la llista X_seal_of_aprovement.

* Prohibeix les connexions dels equips de la llista X_death_note.
 
* Reinicia el proxy i comprova amb els companys que els equips de la llista permesos tenen accés a Internet mitjançant el teu proxy i la resta d'equips no poden accedir.

* Comprova amb el mestre si ell pot accedir des de el seu ordinador a Internet mitjançant el teu proxy. 

* Explica perquè té accés o no amb les teves paraules i mostrant una captura de squid.conf on es demostri el que dius.
 

![](Imagenes/Squid/acl-bien.png)





## 3.- Configurar dominis permesos i prohibits:

Mostra captures que demostrin els següents punts:

* Crea un fitxer anomenat ban.txt amb 10 dominis que no es podran accedir mitjançant el teu proxy.

* Crea una ACL que es digui X_banned_domains (on X és el teu cognom), amb els dominis del fitxer ban.txt.

* Crea una norma que prohibeixi l'accés als dominis de la ACL X_banned_domains als usuaris del proxy.

* Comprova amb dos companys que tinguin accés a connexions al teu proxy que poden accedir a tots els dominis menys els de la llista X_banned_domains.

* Fes la prova amb el mestre.

![](Imagenes/Squid/acl-bien.png)
![](Imagenes/Squid/prohibits.png)
![](Imagenes/Squid/permesos.png)






## 4.- Configurar patrons de url prohibits:

Mostra captures que demostrin els següents punts:

* Crea una ACL anomenada X_banned_words que contingui una llista de paraules (mínim 10).

* Crea una norma que faci que qualsevol url que contingui una paraula de la llista X_banned_words sigui bloquejada pel proxy.

* Demostra-ho amb un company.

* Fes la prova amb el mestre.

![](Imagenes/Squid/Palabras.png)


## 5.- Accés exclusiu:

Mostra captures que demostrin els següents punts:
	
* Crea una ACL amb la IP del mestre.
	
* Crea una ACL amb la URL /var/www/html/mestre/index.html on aquest index.html serà el teu CV.

* Fes que només la IP del mestre pugui accedir a la url.
	
* Demostra-ho amb un company.
	
* Demostra-ho amb l'equip local del proxy.
	
* Demostra-ho amb el mestre.


![](Imagenes/Squid/mestre.png)




