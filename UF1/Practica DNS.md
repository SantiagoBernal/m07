﻿# Pràctica DNS Windows Server

Has de ficar una captura dels punts on hi hagi un **(*)**

## Prerequisits:

En aquesta primera pràctica necessitaràs els següents elements:

* Màquina virtual Windows 2016 server amb la funció de DNS instal·lada (que farà de servidor DNS).
* Màquina virtual Windows 7, 8 o 10 a escollir per tu (que farà de client). 

Les MVs estaran en **xarxa NAT**.

La IP del servidor serà 10.0.2.X on X serà l'últim byte de la IP del teu ordinador real a classe. Per exemple, si el teu ordinador a classe té la IP 172.31.84.44, el servidor tindrà la IP 10.0.2.44 **fixa**.

La IP del client serà 10.0.2.Y on Y serà l'últim byte de la IP del teu ordinador real a classe +100. Per exemple, si el teu ordinador a classe té la IP 172.31.84.44, el client tindrà la IP 10.0.2.144 **fixa**.

Comprova que les MVs poden fer ping entre elles abans de començar la pràctica.

## 1.- Configuració de zona:

Configuraràs una zona al servidor DNS, però abans ves al servidor DNS: *Administración del servidor->Herramientas->DNS* y a *Propiedades* esborra TOTES les *sugerencias de raíz*.

**(*)** Pots comprobar que el servidor no tradueix cap nom a IP fent un nslookup domini al terminal del servidor. Abans esborra la caché amb **ipconfig /flushdns**. 

Crearàs una *zona de búsqueda directa* que es digui **cognom.com** substituint cognom pel teu cognom real.

## 2.- Configuració de hosts amb IP privada:

Dins de la zona creada faràs un nou *Host (A)* amb el nom de Host *servidor* i la IP de la MV que fa de servidor.

**(*)** Mostra com les has creat.

**(*)** Comprova que pots fer ping al nom del host *servidor.cognom.com* i que la traducció de nom a IP funciona amb nslookup.  

Dins de la zona creada faràs un nou *Host (A)* amb el nom de Host *client* i la IP de la MV que fa de client.

**(*)** Mostra com l'has creat.

**(*)** Comprova que pots fer ping al nom del host *client.cognom.com* i que la traducció de nom a IP funciona amb nslookup.  

## 3.- Configuració de hosts amb IP pública:

Ara faràs un host nou que es dirà *google*, al qual li hauràs de donar una de les IPs reals de Google.com. 

Per a fer-ho ves a una màquina amb DNS configurat i busca una de les IPs de google.com.

**(*)** Comprova que a la màquina virtual client tradueix la direcció del host *google.cognom.com* amb nslookup i ping al nom de host.

També faràs un host que es digui **amazon**, aquest host tindrà la IP real de amazon.com.

**(*)** Comprova que amb el navegador del client pots accedir a amazon.cognom.com i surt la web d'Amazon.

## 4.- Servidors arrel:

Busca informació sobre els servidors arrel i respon les següents preguntes, a més possa un enllaça a la font d'informació que heu utilitzat:

**(*)** Què són els servidors arrel?

**Un servidor raiz es wl primer paso en la traducción de direcciones ip**

**(*)** Quines són les IPs, noms i les empreses que gestionen els servidors arrel?

**(*)** Quins servidors arrel hi ha a les illes Hawai?

**(*)** Ara faràs una prova, executa nslookup *yahoo.fr* des del client de la teva xarxa NAT i mostra que no es pot resoldre el nom. 

Afegiràs manualment els servidors arrel al servidor DNS, per això aniràs al servidor DNS -> Propiedades -> sugerencias de raíz i seleccionaràs **cooiar desde el servidor** utilitzant 8.8.8.8 com a font.

El més probable es que no tinguis les IPs dels servidors, així que les introduïràs manualment ara que ja coneixes les seves IPs.

**(*)** Mostra les *sugerencias de raíz* un cop configurades.

**(*)** Ara faràs una prova, executa nslookup *yahoo.fr* des del client de la teva xarxa NAT i mostra que si es pot resoldre el nom. 

**(*)** Quan s'utilitzen els servidors arrel? Ordena les següents opcions des d'abans a després:

* Zona.
* Cache.
* Servidors arrel.
* Reenviadors.

## 5.- Reenviadors:

Ara treuràs els servidors arrel del servidor DNS.

Després configuraràs un reenviador, per això has d'anar a servidor DNS -> Propiedades -> Reenviadores i afegeix 8.8.8.8.

**(*)** Mostra la configuració del reenviador.

Esborra la memòria cache del client i del servidor (el servidor Windows 2016 també té la seva memòria cache, la pots esborrar amb el botó dret sobre el nom del servidor i seleccionant "borrar caché").

**(*)** Prova a fer nslookup a yahoo.fr i alguns dominis més i comprova que es poden resoldre els noms gràcies al reenviador.

**(*)** Què significa quan la resolució de noms ens diu **Respuesta no autorizativa**?

## 6.- Servidors DNS en xarxa (reenviadors):

Per aquesta part de la pràctica canviareu el mode de xarxa de les MV a **adaptador pont** d'aquesta manera podreu comunicar les MV entre vosaltres.

**(*)** La idea es que busqueu un parell de companys i intenteu fer una petició al vostre servidor DNS des del vostre client amb el nom "client.cognomCompany.com" i veieu que no es pot resoldre la petició.

**(*)** Després creeu un reenviador des del vostre servidor al servidor del company (demaneu la seva IP) i proveu de resoldre la direcció "client.cognomCompany.com". Aquesta vegada sí hauria de funcionar.


