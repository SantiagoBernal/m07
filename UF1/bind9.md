# Instalación de servidor DNS BIND9:

**Bind9** és un servidor de DNS muy popular, en esta práctica crearemos una máquina virtual nueva para instalarlo en modo **Adaptador puente**.

## Instalación de BIND9 en Ubuntu 18.04 LTS:

Para instalar bind actualizaremos los paquetes de Linux:

`sudo apt upgrade`

Y posteriormente instalamos Bind9:

`sudo apt install bind9`

Los ficheros de configuración de Bind9 estarán en **/etc/bind**

Ahora vamos a poner como servidor DNS de nuestra máquina virtual su propia dirección IP, para ello cambiamos la dirección DNS por su propia IP y actualizamos los cambios.

## Comprobación de DNS y caché

Para comprobar los cambios realizados podemos ejecutar en un terminal:

`systemd-resolve --status`

Y si hacemos scroll hasta el final nos saldrían las IPs que tenemos configuradas como servidores DNS, debería estar nuestra IP.

Para ver la caché DNS de nuestro equipo podemos ejecutar:

`systemd-resolve --statistics`

Para eliminar la caché DNS de nuestro equipo podemos ejecutar:

`systemd-resolve --flush-caches`

## Ficheros de configuración:

### /etc/bind/named.conf.local

![named.conf.local_1](/images/named.conf.local1.jpg)

En aquest fitxer configuraràs les zones del DNS, a l'exemple tenim la zona aula84.com, però tu li diràs COGNOM.com amb el teu propi cognom.

A la zona especifiques que la zona és de tipus **master**.

La següent línia, on fica **file** és la ruta al fitxer on són els registres de la zona.

### /etc/bind/db.aula84.com

Aquest fitxer tindrà el nom de la zona creada, en el teu cas es dirà **db.COGNOM.com**, per a no escriure des de zero copiaràs un fitxer que ja existeix **db.local**:

`cp db.local db.COGNOM.com`

Amb això tindràs un fitxer que haurà de quedar així:

![db.aula84.com](/images/db.aula84.com.jpg)

En aquest fitxer has configurat dos registres:
    
* servidor - IP MV servidor
* client   - IP MV client

En el cas de servidor hauràs de ficar la IP de la MV que faci de servidor, en el cas del client serà la IP de la MV que facis servir com a client DNS.

Cada vegada que facis un canvi podràs comprovar que els fitxers estan ben escrits amb les instruccions de terminal:

`named-checkconf` 
`named-checkzone zonename db.COGNOM.com`

Si no hi ha resposta vol dir que estan ben escrits els fitxers de configuració (no que siguin correctes), si hi ha algun error ho indicarà per terminal.

A més quan es modifica la configuració del servidor has de reiniciar el servei:

`sudo service bind9 restart`

Ara ja podràs comprovar amb **dig** o **nslookup** i el nom servidor.COGNOM.com si el servei està funcionant, recorda que has de possar com a servidor DNS de les teves màquines virtuals la IP de la MV que faci de servidor DNS.

Comprova amb una de les comandes anteriors si funciona bé el server DNS per als registres que has configurat.

Existeixen diferents tipus de registres: A (Address), MX (Mail eXchanger), CNAME (Canonical Name), NS (Name Server), SOA (Start Of Authority).

* A (Address): Defineix una direcció IP i un nom assignat a un equip (host).
* CNAME (Canonical Name): Es tracta d'un alias que se li dona a un equip que ja té un nom i una IP vàlids. Cada equip pot tindre diferents alias.
* NS (Name Server): Defineis els servidors DNS principals d'un domini, ha d'haver almenys un.
* MX (Mail eXchanger): Busca que vol dir aquest tipus de registre i contesta al final de la pràctica.
* SOA (Start Of Authority): Busca que vol dir aquest tipus de registre i contesta al final de la pràctica. 

Com podeu veure a la captura també s'han de definir una serie de paràmetres com són Serial, Refresh, Retry, Expire o TTL, aquests paràmetres indiquen:

* Serial: Un identificador del fitxer, pot ser qualsevol valor però es recomana que tingui l'estructura AAAMMDD, per exemple si el modifiquem a dia 25-11-2019 hauria de ser **20191125**.
* Refresh: Número de segons que espera un servidor DNS secundari per comprovar els valors d'un registre.
* Retry: Número de segons que espera un servidor DNS secundari per a reintentar recuperar dades del servidor primari després d'un error.
* Expire: Investiga què vol dir aquest paràmetre i contesta a la part final de la pràctica.
* TTL: Investiga què vol dir aquest paràmetre i contesta a la part final de la pràctica.

(*) Canvieu el paràmetre serial per la vostra data d'aniversari en format AAAAMMDD, deixeu la resta de paràmetres per defecte.

Recordeu comprovar 1ue el fitxer està correcte cada vegada que el modifiqueu amb:

`named-checkzone zonename db.COGNOM.com`

I reiniciar el servidor:

`sudo service bind9 restart`

### /etc/bind/named.conf.local

Tornem al fitxer named.conf.local, aquesta vegada per dir al servidor que tindrem una zona inversa, això serveix per a que el servidor ens retorni el nom d'un host quan li preguntem per la seva IP, que es la resolució inversa de la que estem veient fins ara.

Per a fer això creem una zona nova:

![named.conf.local_2](/images/named.conf.local2.jpg)

Com veiem es fa exactament igual que quan declarem una zona per a un domini, però aquesta vegada copiarem el fitxer **db.127** per al nostre fitxer:

`sudo cp /etc/bind/db.127 /etc/bind/db.192`

On **1.168.192** són els tres primers bytes de la teva IP escrits al revès, és probable que a l'aula la teva IP sigui 172.x.y.z, recorda de ficar la MV en adaptador pont.

## /etc/bind/db.192

Ahora deberás configurar el nuevo fichero que has creado db.192, que era una copia de db.127:

`sudo nano db.192`

Dentro del fichero deberás configurar los mismos hosts que configuraste en db.COGNOM.com:

![db.192](/images/db.192.jpg)

En este ejemplo se han configurado dos hosts, 230 y 1 servidor y gateway respectivamente, solo indicamos el último byte de la IP del host, el resto lo sacará del nombre de la zona.

Así 230 apunta al host 192.168.1.230 y se identifica con el nombre de host servidor.aula84.com.

Para comprobar que funciona la resolución inversa podéis probar con el comando:

`dig -x IP a probar`

Ahora que ya has configurado la zona inversa aprovecharás tus extensos conocimientos de BIND para crear un registro y un registro inverso para cada compañero de clase, incluye las capturas de tus ficheros de configuración y prueba que puedes acceder a ellos vía ping + nombre de host y vía dig -x IP de host.


## Preguntes:

1. Investiga què vol que una zona és de tipus "master"?
2. Quins tipus de servidors DNS hi ha configurables a BIND9?
3. Què vol dir que un registre és MX?
4. Què vol dir que un registre és SOA?
5. Què representa el paràmetre Expire d'una zona?
6. Què representa el paràmetre TTL d'una zona?
7. Què significa que un registre sigui PTR?
8. Què volen dir les sigles de BIND?
9. Quin tipus de llicencia té BIND?

## Captures:

1. Fitxer named.conf.local

![named.conf.local_1](Imagenes/Captura_de_2019-12-03_16-34-37.png)

2. Fitxer db.COGNOM.com

![BERNAL.COM](Imagenes/Captura_de_2019-12-03_16-35-50.png)

3. Fitxer db.192 (o el nom que li hagis possat al fitxer de zona inversa)

![DB.192](Imagenes/Captura_de_2019-12-03_16-36-46.png)

4. dig (sencer) on es mostri que funciona la resolució de nom.

![DIG1](Imagenes/Captura_de_2019-11-27_18-16-27.png)

5. dig (sencer) on es mostri que funciona la resolució de IP.

![DIG2](Imagenes/Captura_de_2019-12-03_16-48-34.png)
