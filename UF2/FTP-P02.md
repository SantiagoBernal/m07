# FTP pràctica 2 amb ProFTPD:

En aquesta pràctica seguiràs utilitzant el servidor FTP de la pràctica anterior.

A més et connectaràs al servidor utilitzant el client Filezilla des d'un ordinador amb Windows. [Tens el client aqui](https://filezilla-project.org/download.php).

1. Instal·la el client Filezilla a un equip amb Windows.
2. Crea un usuari anomenat socX (on X és el teu nom) i fes que la seva velocitat de descàrrega del servidor sigui de 10KBs.
3. Descarrega un fitxer de 1 MB o més del server utilitzant Filezilla amb aquest usuari i mostra la velocitat de descàrrega.

![](Imagenes/PROFTPD-2/Ancho_de_banda.png)

4. Crea una cuota per a l'usuari amb un tope de pujada de 10MB i de baixada de 100MB.
5. Demostra que l'usuari té la quota activada (amb la comanda corresponent).
6. Fes que l'usuari exedeixi de la cuota i aixó impedeixi que puji o descarregui fitxers.
7. Mostra el log de les cuotes on es veu el que ha pasat.
8. Com podem fer que les cuotes afectin independentment a cada sessió i no siguin acumulatives?
9. Fes que el servidor FTP admeteixi usuaris anònims sota el pseudònim anonymous i accedeixin a un directori que es digui /var/inisfree. 
10. Fes que els usuaris anònims puguin descarregar fitxers però no pujar-ne cap.
11. Demostra-ho.
12. Fi.
