# FTP:

En aquesta primera pràctica instal·lareu un servidor FTP a una MV Ubuntu (podeu fer servir la mateixa de SQUID, Apache...).

Heu de fer les següents accions i demostrar-les amb captures:

1.- Instal·lar ProFTPD a la MV. Teniu un tutorial [aqui](https://dungeonofbits.com/instalacion-y-configuracion-del-servidor-ftp-proftpd.html).


2.- Fes que qualsevol usuari que entri al teu FTP accedeixi al seu directori home.

![](Imagenes/PROFTPD/DefaultRoot.png)

3.- Canvia el nom del servidor de FTP al teu cognom.

![](Imagenes/PROFTPD/NombreServer.png)

4.- Personalitza el missatge de benvinguda i el d'error d'accés del teu servidor.

![](Imagenes/PROFTPD/Bienvenides.png)

5.- Accedeix al teu servidor des d'un altre equip i fes:
    
* Un llistat dels fitxers que hi ha.
* Puja un fitxer anomenat X.txt (on X és el teu cognom).
* Descarrega el fitxer X.txt del servidor al teu equip.

![](Imagenes/PROFTPD/5-conectandome.png)


6.- Crea un usuari per a un company que sigui el seu nom (el password també el seu nom) i que provi d'accedir al FTP i pujar un fitxer.

![](Imagenes/PROFTPD/6-Jofre.png)


7.- Canvia les directives del servidor perquè els usuaris del grup public accedeixin al directori /home/public (que hauràs de crear) i la resta d'usuaris accedeixi al seu home.

![] (Imagenes/PROFTPD/home_y_public.png)


8.- Canvia el usuari d'un company al grup public.

9.- Comprova que l'usuari anterior accedeix al directori /home/public.

![](Imagenes/PROFTPD/jofrePublic.png)

10.- Comprova que tu, amb el teu usuari que no està al grup public accedeixes al teu directori home.

![](Imagenes/PROFTPD/home-santi.png)

11.- Canvia les directives de login perquè un usuari d'un company no pugui fer login al teu servidor i el teu usuari si pugui fer login.

![](Imagenes/PROFTPD/Blocked.png)

12.- Prova les directives anteriors.
